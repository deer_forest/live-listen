<?php

namespace app\library;

use app\BaseController;

/**
 * NodeJs类，部分加密数据需要通过NodeJs去解密
 * */
class NodeJs extends BaseController
{
    /**
     * 获取抖音签名
     * @param string $room_id 直播间ID
     * */
    public static function get_dy_signature($room_id)
    {
        $cmd = 'node ' . public_path() . 'nodejs' . DIRECTORY_SEPARATOR . 'get_sign.js ' . $room_id . ' ' . request()->host();
        exec($cmd, $signature);
        if (!$signature || !isset($signature[0])) {
            return false;
        }
        return $signature[0];
    }

    /**
     * 解密抖音数据
     * @param string $data 加密数据
     * */
    public static function decode_dy_data($data)
    {
        $cmd = 'node ' . public_path() . 'nodejs' . DIRECTORY_SEPARATOR . 'decode.js ' . base64_encode($data);
        @exec($cmd, $result);
        if (!isset($result) || !isset($result[0])) {
            return [];
        }
        return json_decode($result[0], true);
    }
}