<?php

namespace app\library;

class Redis extends \think\cache\driver\Redis
{
    /**
     * 插入数据
     * @access public
     * @param string $name  缓存变量名
     * @param mixed  $value 数据
     * @return bool
     */
    public function lpush(string $name, $value): bool
    {
        return $this->handler->lPush($name, $value);
    }

    /**
     * 获取队列后20条数据
     * @access public
     * @param string $name   缓存变量名
     * @param int    $length 数据长度
     * @return array
     */
    public function getList(string $name, int $length = 20)
    {
        $data = [];
        for($i=0; $i<$length; $i++) {
            $val = $this->handler->lPop($name);
            if (!$val) {
                break;
            }
            $data[] = $val;
        }
        return $data;
    }

    /**
     * 设置键过期时间
     * @access public
     * @param string $name  缓存变量名
     * @param int    $time  过期时间
     * @return bool
     * */
    public function expire(string $name, int $time) : bool
    {
        return $this->handler->expire($name, $time);
    }
}