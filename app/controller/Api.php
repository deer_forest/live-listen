<?php

namespace app\controller;

use app\BaseController;

class Api extends BaseController
{
    /**
     * 异步shell
     * @param string $key 键名
     * */
    public function sync_shell($key)
    {
        if ($key) {
            $cmd = 'php ' . root_path() . 'think LiveListen ' . $key;
            if (strpos(PHP_OS, "Linux") !== false) {
                exec($cmd . ' &');
            } else if (strpos(PHP_OS, "WIN") !== false) {
                exec('start /b ' . $cmd);
            }
        }
        $this->success('请求成功');
    }
}