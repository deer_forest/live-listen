const puppeteer = require('puppeteer');

var arguments = process.argv.splice(2);
var room_id = arguments[0];
var host = arguments[1];

const url = "http://" + host + '/nodejs/sign?room_id=' + room_id;

(async () => {
    // 启动浏览器
    const browser = await puppeteer.launch({
        headless: true,
        args: [
            '--no-sandbox',
            '--disable-setuid-sandbox'
        ]
    });
    // 创建一个新页面
    const page = await browser.newPage();

    // 访问一个网址
    await page.goto(url);

    // 获取抖音signature
    const signature = await page.$eval("#signature", el => el.innerHTML)

    // 关闭浏览器
    await browser.close();

    console.log(signature);
})();