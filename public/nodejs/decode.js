const wss_pb = require('./proto/wss_pb');
const message_pb = require('./proto/message_pb');
const zlib = require('zlib');

var arguments = process.argv.splice(2);
var message = arguments[0];

var data = proto_decode(message);
console.log(JSON.stringify(data));

function proto_decode(base64_str) {
	var arr = new Uint8Array(atob(base64_str).split('').map(char => char.charCodeAt(0)));
	var obj = wss_pb.WssResponse.deserializeBinary(arr).toObject();
	var payload = zlib.gunzipSync(Buffer.from(obj.payload, 'base64')).toString('base64');
	var messagesList = message_pb.Response.deserializeBinary(payload).toObject().messagesList;

	var data = [];
	for (var i = messagesList.length - 1; i >= 0; i--) {
		var res = [];
		switch(messagesList[i]['method']) {
			case 'WebcastChatMessage': // 弹幕消息
				res = message_pb.ChatMessage.deserializeBinary(messagesList[i]['payload']).toObject();
				data.push({
					'type': 'ChatMessage',
					'user': {
						"id": res.user.id,
						"shortid": res.user.shortid,
						"nickname": res.user.nickname,
						"gender": res.user.gender,
						"level": res.user.level,
						"birthday": res.user.birthday,
						"telephone": res.user.telephone,
						"avatarthumb": res.user.avatarthumb,
						"displayid": res.user.displayid
					},
					'content': res.content
				});
				break;
			case 'WebcastMemberMessage': // 用户消息
				res = message_pb.MemberMessage.deserializeBinary(messagesList[i]['payload']).toObject();
				data.push({
					'type': 'MemberMessage',
					'user': {
						"id": res.user.id,
						"shortid": res.user.shortid,
						"nickname": res.user.nickname,
						"gender": res.user.gender,
						"level": res.user.level,
						"birthday": res.user.birthday,
						"telephone": res.user.telephone,
						"avatarthumb": res.user.avatarthumb,
						"displayid": res.user.displayid
					},
					'content': '来了'
				});
				break;
			case 'WebcastLikeMessage': // 点赞消息
				res = message_pb.LikeMessage.deserializeBinary(messagesList[i]['payload']).toObject();
				data.push({
					'type': 'LikeMessage',
					'user': {
						"id": res.user.id,
						"shortid": res.user.shortid,
						"nickname": res.user.nickname,
						"gender": res.user.gender,
						"level": res.user.level,
						"birthday": res.user.birthday,
						"telephone": res.user.telephone,
						"avatarthumb": res.user.avatarthumb,
						"displayid": res.user.displayid
					},
					'content': '点赞了'
				});
				break;
			case 'WebcastGiftMessage': // 礼物消息
				res = message_pb.GiftMessage.deserializeBinary(messagesList[i]['payload']).toObject();
				data.push({
					'type': 'GiftMessage',
					'user': {
						"id": res.user.id,
						"shortid": res.user.shortid,
						"nickname": res.user.nickname,
						"gender": res.user.gender,
						"level": res.user.level,
						"birthday": res.user.birthday,
						"telephone": res.user.telephone,
						"avatarthumb": res.user.avatarthumb,
						"displayid": res.user.displayid
					},
					'gift': {
						'id': res.gift.id,
						'name': res.gift.name,
						'image': res.gift.image
					},
					'content': res.gift.describe
				});
				break;
			case 'WebcastSocialMessage': // 关注消息
				res = message_pb.SocialMessage.deserializeBinary(messagesList[i]['payload']).toObject();
				data.push({
					'type': 'SocialMessage',
					'user': {
						"id": res.user.id,
						"shortid": res.user.shortid,
						"nickname": res.user.nickname,
						"gender": res.user.gender,
						"level": res.user.level,
						"birthday": res.user.birthday,
						"telephone": res.user.telephone,
						"avatarthumb": res.user.avatarthumb,
						"displayid": res.user.displayid
					},
					'content': '关注了'
				});
				break;
			default:
				res = [];
				break;
		}
	}
	return data;
}