一、获取抖音signature的环境搭建说明
    1、npm i 安装node环境
    2、安装谷歌浏览器
        配置下载源
            vi /etc/yum.repos.d/google-chrome.repo
            [google-chrome]
            name=google-chrome
            baseurl=http://dl.google.com/linux/chrome/rpm/stable/$basearch
            enabled=1
            gpgcheck=1
            gpgkey=https://dl-ssl.google.com/linux/linux_signing_key.pub
        执行按装命令
            yum -y install google-chrome-stable --nogpgcheck
        验证是否安装成功。
            google-chrome --version